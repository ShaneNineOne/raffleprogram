//Program made by: Shane Roberts
import javax.swing.JOptionPane;
public class Raffle {
	
	public static void main(String[] args) {
		boolean repeat = true;
		JOptionPane Joption = new JOptionPane();
		String winningNumbers = JOptionPane.showInputDialog("Please enter the winning numbers.");
		//NOTE: This will fail only if the user clicks cancel, in which case, we bypass the loop outright.
		if (!verifyInput(winningNumbers)) repeat = false;
	
		//Proceed once the winning numbers are verified, only if they are verified.
		while (repeat && !verifyNumber(winningNumbers)) {
			winningNumbers = JOptionPane.showInputDialog("Please enter a valid configuration of winning numbers!");
		}

		int firstPlaceWinners = 0;
		int secondPlaceWinners = 0;
		int thirdPlaceWinners = 0;
		
		//Loop while there's still tickets left to enter...
		while (repeat) {
			
			String currentName = Joption.showInputDialog("Please enter the contestant's name.");
			if (!verifyInput(currentName)) break;
			
			String currentEntry = Joption.showInputDialog("Please enter the ticket number.");
			if(!verifyInput(currentEntry)) break;

			//Don't advance until we verify the current ticket number
			while (!verifyNumber(currentEntry)) {
				currentEntry = Joption.showInputDialog("Please enter a valid ticket number!");
			}
			
			Ticket newTicket = new Ticket(currentName, currentEntry);
			int victoryType = newTicket.getWinnerType(winningNumbers);
			
			if (victoryType == 1) {
				Joption.showMessageDialog(null, newTicket.getName() + " was a First Place Winner with \nticket number " + newTicket.getTicketNumber() + ".");				
				firstPlaceWinners++;
			} else if (victoryType == 2) {
				Joption.showMessageDialog(null, newTicket.getName() + " was a Second Place Winner with \nticket number " + newTicket.getTicketNumber() + ".");				
				secondPlaceWinners++;
			} else if (victoryType == 3) {
				Joption.showMessageDialog(null, newTicket.getName() + " was a Third Place Winner with \nticket number " + newTicket.getTicketNumber() + ".");								
				thirdPlaceWinners++;
			} else {
				Joption.showMessageDialog(null, newTicket.getName() + " was not a winner with ticket number " + newTicket.getTicketNumber() + ".");
			}
			
			int result = JOptionPane.showConfirmDialog(null, "Would you like to enter another ticket?", "Enter another ticket?", Joption.YES_NO_OPTION);
			
			if (result == 1) {
				repeat = false;
			}
		}
		
		Joption.showMessageDialog(null, "There " + getWereOrWas(firstPlaceWinners) + " " + firstPlaceWinners + " first place winner" + getSIfNeeded(firstPlaceWinners) +", " + secondPlaceWinners + " second place winner" + getSIfNeeded(secondPlaceWinners) + ", and " + thirdPlaceWinners + " third place winner" + getSIfNeeded(thirdPlaceWinners) + ".");
	}
	
	//Validate user input to prevent nasty errors if they don't input the proper sequence for tickets.
	private static boolean verifyNumber(String numberEntry) {
		if (numberEntry == null) return false;
		return numberEntry.length() == 6 && numberEntry.matches("[0-9]+");
	}
	
	private static String getWereOrWas(int num) {
		if (num <= 1) return "was";
		else return "were";
	}
	
	private static String getSIfNeeded(int num) {
		if (num != 1) return "s";
		else return "";
	}
	
	//For handling verification should the user press cancel ever.
	private static boolean verifyInput(String input) {
		return input != null;
	}
}
