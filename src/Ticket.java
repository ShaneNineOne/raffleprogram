//Class made by: Shane Roberts
import java.lang.StringBuilder;
public class Ticket {
	public String name;
	public String ticketNumber;
	
	public Ticket(String enteredName, String enteredTicketNumber) {
		name = enteredName;
		ticketNumber = enteredTicketNumber;
	}
	
	//Returns the contestant's name.
	public String getName() {
		return name;
	}
	
	//Returns the ticket number for the contestant.
	public String getTicketNumber() {
		return ticketNumber;
	}
	
	public int getWinnerType(String winningNumber) {
		//Check if equal for first place.
		if (ticketNumber.equals(winningNumber)) return 1;
		
		//Check if the reverse is equivalent for second place
		if(ticketNumber.equals(new StringBuilder(winningNumber).reverse().toString())) return 2;

		//Check positions 1-3, 2-4, 3-5, and 4-6 for equivalence for third place.
		for (int x = 0; (x + 3) < winningNumber.length(); x++) {
			if (ticketNumber.substring(x, x + 3).equals(winningNumber.substring(x, x + 3))) return 3;
		}

		//The fallback - didn't win anything
		return 0;
	}
}
